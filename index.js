const _ = require('lodash');
const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const axios = require('axios');
const cors = require('cors');
require('dotenv').config();
const port = process.env.PORT;

app.use(cors({
    origin: (origin, callback) =>  callback(null, true)
}));
app.use(bodyParser.json());

const {SALESFORCE_URL} = process.env;
const CONTACT_QUERY = `SELECT RecordType.Name,RecordType.Id,Name,MobilePhone,Email,Gatuadress__c,Id,Grupp__c,Kategori__c,Typ__c FROM Contact`;
const FIKAGRUPP_QUERY = `SELECT Name,Gruppledare__c,ID_nr__c,Id FROM Grupp__c`;
const MEETS_QUERY = `SELECT Name,Grupp__c,Datum__c,V_rd_f_r_fikat__c,Gatuadress_ort_portkod__c FROM Traff__c`;
const RECORD_TYPES_QUERY = `SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Contact'`;
const CHAT_QUERY = (id) => `SELECT Grupp__c, Grupp__r.name, Message__c, Sender__r.name, Sender__c, CreatedDate FROM Ak_chat__c WHERE Grupp__c = '${id}' ORDER BY CreatedDate ASC NULLS LAST`;
const USERNAME_PASSWORD_QUERY = (email) => `SELECT Ak_Password_Text__c,Id, Grupp__c FROM Contact WHERE Email = '${email}'`;
const PROFILE_QUERY = (id) => `SELECT Name, MobilePhone, Email, RecordType.Id, Gatuadress__c, Grupp__c, Kategori__c, Typ__c FROM Contact WHERE Id = '${id}'`;
const GROUP_QUERY = (id) => `SELECT Name,Gruppledare__c FROM Grupp__c WHERE Id = '${id}'`;
const MEMBER_QUERY = (id) => `SELECT Name, Typ__c, MobilePhone, Id FROM Contact WHERE Grupp__c = '${id}'`;
const GET_MEETS_QUERY = (id) => `SELECT Name, Datum__c, V_rd_f_r_fikat__c, V_rd_f_r_fikat__r.name, Gatuadress_ort_portkod__c FROM Traff__c WHERE Grupp__c = '${id}'`;
const GET_HOST_QUERY = (id) => `SELECT Name FROM Contact WHERE Id = '${id}'`;

function getAccessToken() {
    const {
        SALESFORCE_USERNAME: username,
        SALESFORCE_PASSWORD: password,
        SALESFORCE_CLIENT_ID: clientId,
        SALESFORCE_CLIENT_SECRET: clientSecret,
    } = process.env;

    const data = `grant_type=password&username=${username}&password=${encodeURIComponent(password)}&client_id=${clientId}&client_secret=${clientSecret}`;
    axios.post(SALESFORCE_URL+'/oauth2/token', data)
        .then(res => {
            const {access_token} = res.data;
            if (access_token) {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
            }
        })
        .catch(error => console.log('token error:', error));
}

getAccessToken();

function getQuery(endpoint, query) {
    app.get(endpoint, (req, res) => {
        axios.get(`${SALESFORCE_URL}/data/v48.0/query/?q=${query}`)
            .then(response => {
                /*response.data.records = response.data.records.map(record => {
                    const url = record.attributes.url.split('/');
                    const id = url[url.length - 1];

                    return {...record, id};
                })*/
                res.send(response.data)

            })
            .catch(error => {console.log('contacts error:', error); console.log(error.request.data)})
    });
}

function getMeets(endpoint) {
    app.post(endpoint+'/:Id', (req, res) => {
        axios.get(`${SALESFORCE_URL}/data/v48.0/query/?q=${GET_MEETS_QUERY(req.params.Id)}`)
            .then(response => {
                /*const Date = _.get(response, 'data.records[0].Datum__c');
                const Address = _.get(response, 'data.records[0].Gatuadress_ort_portkod__c');
                const Host = _.get(response, 'data.records[0].V_rd_f_r_fikat__c');

                Meet = {
                    Date: Date,
                    Address: Address,
                    Host: Host
                }*/
                res.send(response.data);
            })
            .catch(error => console.log('Error ',error))

    })
}

function getChat(endpoint) {
    app.get(endpoint+'/:Id', (req, res) => {
        axios.get(`${SALESFORCE_URL}/data/v48.0/query/?q=${CHAT_QUERY(req.params.Id)}`)
            .then(response => {
                console.log(response.data);
                res.send(response.data);
            })
            .catch(error => console.log('Error ',error))

    })
}

/*function getHost (endpoint) {
    app.post(endpoint+'/:Id', (req, res) => {
        axios.get(`${SALESFORCE_URL}/data/v48.0/query/?q=${GET_HOST_QUERY(req.params.Id)}`)
            .then(response => {
                console.log(response);
                const Name = _.get(response, 'data.records[0].Name');

                Host = {
                    Name: Name
                }
                res.send(Host);
            })
            .catch(error => console.log('Error ', error))
    })
}*/

function getGroup(endpoint) {
    app.post(endpoint+'/:Id', (req, res) => {
        axios.get(`${SALESFORCE_URL}/data/v48.0/query/?q=${GROUP_QUERY(req.params.Id)}`)
            .then(response => {
                console.log(response.data.recotds);
                const Name = _.get(response, 'data.records[0].Name');
                const Leader = _.get(response, 'data.records[0].Gruppledare__c');
                const Id = _.get(response, 'data.response[0].Id')

                const Group = {
                    Name: Name,
                    Leader: Leader,
                    Id: Id
                }

                res.send(Group);
                

            })

            .catch(error => console.log('Error: ', error))
    })
}

function getMembers(endpoint) {
    app.post(endpoint+'/:Id', (req, res) => {
        axios.get(`${SALESFORCE_URL}/data/v48.0/query/?q=${MEMBER_QUERY(req.params.Id)}`)
            .then(response => {
                res.send(response.data);
            })
            .catch(error => console.log('Error: ',error))
    })
}

function checkPasswordQuery(endpoint) {
    app.post(endpoint, (req, res) => {
        axios.get(`${SALESFORCE_URL}/data/v48.0/query/?q=${USERNAME_PASSWORD_QUERY(req.body.Email)}`)

            .then(response => {
                const passwordCheck = _.get(response, 'data.records[0].Ak_Password_Text__c')
                const profileId = _.get(response, 'data.records[0].Id')
                const groupId = _.get(response, 'data.records[0].Grupp__c')
                    //const passwordCheck = response.data.records.Ak_Password__c;
                    console.log(passwordCheck);
                    let correctPassword = false;
                    console.log('password set');
                    if(passwordCheck && req.body.Ak_Password__c === passwordCheck){
                        console.log('password true')
                        correctPassword = true;
                    }
                        
                        const check = {
                            boolean: correctPassword,
                            id: profileId,
                            gruppId: groupId
                        };
                    res.send (check);
                
                //res.send(response.data)

            })
            .catch(error => console.log('contacts error:', error))
    });
}

function getProfile(endpoint) {
    app.post(endpoint+'/:Id', (req, res) => {
        axios.get(`${SALESFORCE_URL}/data/v48.0/query/?q=${PROFILE_QUERY(req.params.Id)}`)
            .then(response => {
                
                const Name = _.get(response, 'data.records[0].Name');
                const Email = _.get(response, 'data.records[0].Email');
                const Mobile = _.get(response, 'data.records[0].MobilePhone');
                const Address = _.get(response, 'data.records[0].Gatuadress__c');
                const Type = _.get(response, 'data.records[0].Typ__c');
                const Group = _.get(response, 'data.records[0].Grupp__c');
                const Category = _.get(response, 'data.records[0].Kategori__c');
                const RecordType = _.get(response, 'data.records[0].RecordType.Id');
                
                
                const Profile = {
                    Name: Name,
                    Email: Email,
                    Mobile: Mobile,
                    Address: Address,
                    Type: Type,
                    Group: Group,
                    Category: Category,
                    RecordType: RecordType
                };
                res.send (Profile);
            })
            .catch(error => console.log('contacts error: ', error))
    });
}

function patchProfile(endpoint) {
    app.post(endpoint+'/:Id', (req, res) => {
        patchQuery('Contact', req.params.Id, req.body)
        res.send (console.log('success'))
    })
}

function postMessage(endpoint, sobject) {
    console.log('hello');
    app.post(endpoint, (req, res) => {
        console.log(req.body);
        axios.post(`${SALESFORCE_URL}/data/v48.0/sobjects/${sobject}/`, req.body)
        .then(response => res.send(response.data))
        .catch(error => res.status(_.get(error, 'status', 500)).send(error))
    })
}

function postRequest(endpoint, sobject) {
    app.post(endpoint, (req, res) => {
        console.log(req.body)
        axios.post(`${SALESFORCE_URL}/data/v48.0/sobjects/${sobject}/`, req.body)
            .then(response => res.send(response.data))
            .catch(error => res.status(_.get(error, 'status', 500)).send(error))
    });
}
/*function checkRequest(endpoint) {
    app.get(endpoint, (req, res) => {

        checkPasswordQuery('/contacts', USERNAME_PASSWORD_QUERY(req.Email), req.Ak_Password__c)
    });

}*/

function pushQuery(sobject, data){




}

function patchQuery(sobject, id, data){
    console.log(id);
    console.log(data);
    axios.patch(`${SALESFORCE_URL}/data/v48.0/sobjects/${sobject}/`+ id,  data)
        .then(res =>
            console.log(res)
        )
        .catch(errors =>
            console.log('Errors: '+errors)
        )
    }
//setTimeout(() => pushQuery('Contact',{LastName: 'Jama',FirstName: 'Boe', Kategori__c: 'Volontär',Typ__c: 'Chaufför'}), 1000)
//setTimeout(() => patchQuery('Contact', '0039E000014N7InQAK', {FirstName: 'Obunga', LastName: 'Llama'}), 1000)
getQuery('/contacts', CONTACT_QUERY);
getQuery('/fikagrupper', FIKAGRUPP_QUERY);
getQuery('/records', RECORD_TYPES_QUERY);
getQuery('/chat', CHAT_QUERY);


getQuery('/meet', MEETS_QUERY);



postRequest('/contact', 'Contact');
checkPasswordQuery('/login');

getProfile('/profil');
patchProfile('/profil_patch');

getGroup('/grupper');
getMembers('/members');

getMeets('/meets');
//getHost('/host')

getChat('/chat');
postMessage('/send','Ak_chat__c');

app.listen(port, () => console.log(`Listening at http://localhost:${port}`))
