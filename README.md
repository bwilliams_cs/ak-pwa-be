# Äldrekontakt PWA Backend Service 

Still very much Work In Progress.

## API

Currently available calls:
```
GET /contacts
GET /fikagrupper
GET /meets
```

## How to run
```bash
npm start
```

Will run the service on port 1234. So when running locally, you can use `localhost:1234/contacts` to get contacts.

Format:
```json
{
    "totalSize": 0,
    "done": true,
    "records": []
}
```



